# GDK-in-a-box

[[TOC]]

## What is GDK-in-a-box?

GDK-in-a-box is Virtual Machine which is preconfigured to run the
[GitLab Development Kit (GDK)](https://gitlab.com/gitlab-org/gitlab-development-kit),
so that [Everyone Can Contribute](https://handbook.gitlab.com/handbook/company/mission/#mission) to GitLab with ease!

## What's in the box?

A whole stack of stuff that makes it easy for you to contribute to the GitLab source code!

- A virtual machine
- ...preconfigured with a [GitLab platform instance](https://about.gitlab.com/platform/) running [GitLab Remote Development Workspace instance](https://docs.gitlab.com/ee/user/workspace/) on a [Rancher Desktop Kubernetes cluster](https://www.rancher.com/products/rancher-desktop)
- ...with the [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit) and GitLab source code running [in the workspace](https://gitlab.com/gitlab-org/gitlab-development-kit/-/tree/main/support/gitlab-remote-development)
- ...editable using the [GitLab Web IDE](https://docs.gitlab.com/ee/user/project/web_ide/) via a port exposed on the Virtual Machine

## What does the box do for me?

- You can work on the GitLab source code using the latest GDK, without having to install the GDK (and all its dependencies) manually or locally.
- You don't have to install docker or other containerization tools, or deal with containers, or any local dependencies. The only dependency you need on your machine (or cloud provider) is Virtual Machine software (or a cloud account which can run Virtual Machines) to run the GDK-in-a-box virtual machine image!
- It helps [advance GitLab's contributor success efforts](https://gitlab.com/gitlab-org/developer-relations/contributor-success/team-task/-/issues/360).

## Using the box

1. Download and install [VirtualBox](https://www.virtualbox.org/) or [VMWare Workstation Player](https://www.vmware.com/products/workstation-player/workstation-player-evaluation.html). If you are on an Apple Silicon Mac, you can download [VMWare Fusion Player](https://customerconnect.vmware.com/evalcenter?p=fusion-player-personal-13)
2. Download and run the GDK-in-a-box VirtualBox or VMWare image (TODO: Add links once built)
3. Connect to port NNNN on the virtual machine, which will be running the GitLab Web IDE  in a Remote Development workspace.
4. Run `gdk update` in the terminal to ensure you have the latest version of the GitLab source code.
5. Start contributing! You can use the Web IDE and its terminal in the workspace to:
    - Browse, explore, test, and modify the running development instance of GitLab within the GDK codebase
    - Add your fork of the GitLab repo as a remote in the local gitlab cloned repo, so you can commit and push your changes, and open a Merge Request to contribute them. All from within the Web IDE!

## Building the box

See the [building](./building.md) page for details on how the virtual machine is built and maintained.

## Current status of the box

This is still in proof-of-concept phase. Here is a high-level plan of the iteration goals:

- Manually create a VMWare VM which can be run on VMWare Fusion, and document all steps for the creation. Fusion was picked because it was the only VM software which met these two criteria:
  - Can run ARM64 architectures and thus be used locally on M1 MacOS computers (the standard for internal GitLab, and thus the best target for initial development and dogfooding).
  - Has a free "player" version to run the VMs, and thus be used by community contributors without purchasing a license.
- Convert the manual steps to Packr, in order to automate the creation
- Add other VM hosting targets and architectures for Packr in addition to VMWare Fusion:
  - VMWare (AMD64 architecture)
  - Parallels (AMD64 and ARM64 architectures)
  - VirtualBox (AMD64 architectures, ARM64 is not yet fully supported)
  - Google Cloud
  - AWS
  - Azure
- Set up all of the above to auto-build and auto-release on GitLab CI/CD, with a new version every release (or week, or day, or continuously)
- Create a version of all of the above which installs the GDK instead of GitLab Omnibus in the VM, so that "local" development on GitLab can be done in the VM. This will be mostly useful for developing/testing the Remote Development feature, or other GitLab features which may be difficult to develop within a Remote Development workspace due to too many layers of "inception" virtualization layering.

# Building GDK-in-a-box

[[TOC]]

## Manual Steps

- Download and install:
  - VirtualBox: https://www.virtualbox.org/ (free, works everywhere except Apple Silicon)
  - ...or VMWare Fusion Pro: https://www.vmware.com/products/fusion.html (to build VMWare version that will work with https://customerconnect.vmware.com/evalcenter?p=fusion-player-personal-13)
- Download and install an Ubuntu Server image from https://www.osboxes.org/ubuntu-server/ (click correct tab for VirtualBox or VMWare disk image) 
  - (TODO: can build Ubuntu VM from scratch for more security in the future instead of relying on osboxes/etc third party prebuilt base images)
  - NOTE: The download from oxboxes.org is `*.vdi` Virtual Disk Image (VirtualBox) or `*.vmdk` archive (VMWare), which means you will need to create a virtual machine manually and attach the existing disk image to it. (TODO: Add instructions)
  - Install OpenSSH server
  - server/user name: `gdk-in-a-box`, password: `contribute`
- See [manual_setup_steps](./manual_setup_steps.md) for details of the following steps:
  - Install GitLab omnibus on the VM: https://docs.gitlab.com/omnibus/
  - Add an enterprise license so we can run remote dev feature (is this possible/allowed? See [open issues](#open-issues) below)
  - Configure remote development in running GitLab instance, based on https://gitlab.com/gitlab-org/remote-development/gitlab-remote-development-docs/-/blob/main/doc/local-development-environment-setup.md.
  - Fork the GDK project into the local GitLab instance on the VM (TODO: necessary until we can specify devfile + remote repo via API on workspace create - see [open issues](#open-issues)
  - Create a remote development workspace from the GDK project, based on instructions here: https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/gitlab-remote-development.md. Run the bootstrap script and wait for the clone
  - Configure the VM to expose the running workspace port.
- Save a snapshot of the configured VM
- Upload the VM snapshot to a public place (e.g. releases page of this project, or a blobstore, or wherever is best to store a multi-gigabyte file for download)


## Automation

TODO: Automate the above manual steps with a tool such as [Packer](https://developer.hashicorp.com/packer/docs/intro), and set up CI/CD to automatically build a new version for every GitLab release.

## Open Issues

- Can we embed an EE license in the workspace in order to run remote development? Perhaps one which expires in a short time?
- Need to make the remote development workspace max lifetime able to be overridden to be unlimited, so the pre-created workspace will work longer than 120 hours. E.g. allow -1 to configure it to be unlimited - can be directly edited in the database for now, just needs to not invalidate the model validations.
- Ideally would not need to fork the GDK locally. If we could create workspaces by specifying the devfile + remote repo via API that would make it possible.
- VirtualBox has a beta for Apple Silicon, but it is not stable (or even functional, wouldn't run a plain Ubuntu VM). See comments here: https://osxdaily.com/2022/10/22/you-can-now-run-virtualbox-on-apple-silicon-m1-m2/

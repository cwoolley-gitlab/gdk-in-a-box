# Manual Setup Steps

NOTE 1: This contains a single consolidated version of the VM setup with GitLab Omnibus, Rancher Desktop, and https://gitlab.com/gitlab-org/remote-development/gitlab-remote-development-docs/-/blob/main/doc/local-development-environment-setup.md, with all steps included sequentially and in one place.

NOTE 2: These instructions are currently specific to VMWare Fusion and Ubuntu Linux. Other virtual machine environments may differ.

## ENV var summary

This section summarizes all ENV vars which need to be set at various points in different environments.

### Local desktop ENV vars

```sh
export GDKINABOX_VM_IP=<obtain from Virtual Machine Library>
```

Example:

```sh
export GDKINABOX_VM_IP=172.16.17.134
```

## Identify IP address of VM and add as hosts entry

1. In VMWare Fusion menu, select `Window -> Virtual Machine Library`
1. The current IP address will be displayed at the bottom
1. `export GDKINABOX_VM_IP=<obtain from Virtual Machine Library>`
1. On local development environment, edit `/etc/hosts` to add an entry `gdk-in-a-box` for the IP.
1. In future sections, if they end with `...in the VM`, you should run `ssh gdk-in-a-box@gdk-in-a-box` to log into the Virtual Machine.

TODO: Should we attempt to assign a static IP, or use cloud init somehow to make it static?

## Setup no-password sudoers access in the VM

1. `echo "$USER ALL=(ALL:ALL) NOPASSWD: ALL" | sudo tee "/etc/sudoers.d/dont-prompt-$USER-for-sudo-password"`

## Install GitLab omnibus in the VM

1. The following instructions are from https://about.gitlab.com/install/#ubuntu
1. `sudo apt-get update`
1. `sudo apt-get install -y curl openssh-server ca-certificates tzdata perl`
1. Yes to all prompts
1. `curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.deb.sh | sudo bash`
1. `sudo GITLAB_ROOT_PASSWORD="contribute" EXTERNAL_URL="http://gdk-in-a-box" apt install gitlab-ee`
1. `sudo shutdown -h now` and TAKE A SNAPSHOT OF THE VM
1. Reboot and confirm that you can connect to http://gdk-in-a-box from the local machine, and log in as `root`/`contribute`

## Disable enterprise license check for remote development feature

1. TODO: Add steps to modify source under `/opt/gitlab/embedded/service/gitlab-rails`

## Install Docker Engine in the VM

1. The following instructions are from https://ranchermanager.docs.rancher.com/v2.5/getting-started/installation-and-upgrade/installation-requirements/install-docker, which is linked from https://ranchermanager.docs.rancher.com/v2.5/pages-for-subheaders/installation-requirements#installing-docker
1. `curl https://releases.rancher.com/install-docker/20.10.sh | sh`

## Install Single-Node Rancher in the VM

1. The following instructions are from https://ranchermanager.docs.rancher.com/v2.5/pages-for-subheaders/rancher-on-a-single-node-with-docker
1. See differences for ARM64 guest Ubuntu at: https://ranchermanager.docs.rancher.com/v2.5/getting-started/installation-and-upgrade/advanced-options/enable-experimental-features/rancher-on-arm64
1. `sudo docker run -d --restart=unless-stopped -p 8000:80 -p 8443:443 -e CATTLE_BOOTSTRAP_PASSWORD=contribute --privileged rancher/rancher:v2.8.0`
1. Give it a minute to start up
1. From your local development environment, go to https://gdk-in-a-box:8443/ (you will need to click advanced and allow to access the untrusted site)
1. Log in with `contribute` password
1. Accept `https://gdk-in-a-box:8443` as the Rancher URL.

## Install kubectl in the VM

1. `sudo snap install kubectl --classic`
1. From the Rancher web UI at https://gdk-in-a-box:8443/ on your local dev environment, navitate to the `local` cluster
1. At the top right of the Cluster Dashboard, click the `Download KubeConfig` link and save it locally
1. Copy the contents into the VM in a new file at `/home/gdk-in-a-box/.kube/config`
1. Make another copy at `/root/.kube/config`
1. Run `kubectl get all` (without `sudo`) to verify it is properly configured
